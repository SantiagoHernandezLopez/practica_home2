        <!--En este row estan el carousel y el parafo de texto que hay a su derecha. El carousel tiene tres imágenes. -->
        <div id="carouselExampleControls" class="carousel slide col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" data-ride="carousel">
            <div class="carousel-inner">

                <div class="carousel-item active">
                    <img class="d-block" src="img/devil.jpg" alt="Rathalos">

                    <div class="fechas">
                        -26/Enero/2018
                        Lanzamiento mundial de Monster Hunter: World.
                        <p>
                        -5/Enero/2018
                        Último tráiler de Monster Hunter: World revelando una nueva área: ???. Teostra Y Kushala Daora regresan.
                        </p>
                        <p>
                        -30/Octubre/2017
                        Nuevo tráiler de Monster Hunter: World revelando una nueva área: Valle Putrefacto. Se revelan nuevos monstruos: Gran Girros, Radobaan y Odogaron.
                    </div>
                </div>

                <div class="carousel-item">
                    <img class="d-block" src="img/kutku.jpg" alt="Gravios">

                    <div class="fechas">
                        <p>
                        19/Septiembre/2017
                        Nuevo tráiler de Monster Hunter: World revelando una nueva área: Yermo de Agujas. Se revelan nuevos monstruos: Tzitzi-Ya-Ku, Paolumu y Legiana.
                        </p>
                        <p>
                        18/Agosto/2017
                        Nuevo tráiler de Monster Hunter: World revelando una nueva área: Yermo de Agujas. Barroth y Diablos regresan, se revelan nuevos monstruos: Kulu-Ya-Ku y otros.
                        </p>
                    </div>
                </div>

                <div class="carousel-item">
                    <img class="d-block" src="img/raj.jpg" alt="Blangonga">

                    <div class="fechas">
                        <p>
                        5/Julio/2017
                        Actualización a Monster Hunter Frontier Z con la Ruta de Caza que sustituye al Tenrou y que introduce al Fatalis Blanco de rango G.
                        </p>
                        <p>
                        12/Junio/2017
                        Anuncio del último juego de la saga, Monster Hunter: World. Inicio de la Quinta Generación.
                        </p>
                        <p>
                        26/Mayo/2017
                        Anuncio del lanzamiento de MH XX HD en Switch.
                    </div>
                </div>
            </div>
        </div>

        <!--Esta es la barra de búsqueda que esta en medio de la pagina. no es funcional. -->
        <div class="topnav" align="center">
            <input type="text" placeholder="Buscar..." id="buscador">
        </div>

            <!--Esta es la parte de la pagina en la que estan las imagenes y una pequeña descripcion debajo de ellas. Después de las descripciones hay unos botones los cuales llevan a un apartado relacionado en una wiki -->
            <div class="row justify-content-around" id="articulos" align="center">
                <div class="title col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" align="center">
                    <h1>Articulos de interés</h1>
                </div>

                <!--columna 1-->
                <div class="articulo col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 ">
                    <div class="articulo-imagen justify-content-center">
                        <img src="img/narga.png">
                    </div>

                    <div class="articulo-texto justify-content-center" align="center">
                        <p><h3>Bestiario</h3></p>
                        <p>Monsters are the central aspect of the Monster Hunter series. Monsters are (by Monster Hunter definition) creatures in the World of Monster Hunter. Many look natural, but some of them have a mythical appearance, and some of them possess mystical, supernatural, and even divine powers.</p>
                    </div>

                    <div class="articulo-boton justify-content-center">
                        <a class="btn btn-secondary" href="http://monsterhunter.wikia.com/wiki/Monster_Portal" target="blank" role="button" >Leer más...</a>
                    </div>
                </div>

                <!--columna 2-->
                <div class=" articulo col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 ">
                    <div class="articulo-imagen justify-content-center">
                        <img src="img/ls.png">
                    </div>

                    <div class="articulo-texto justify-content-center" align="center">
                        <p><h3>Tipos de armas</h3></p>
                        <p>There are 16 types of weapons in the Monster Hunter universe, with three of them (MBG, CB, IG) exclusive to the main series, and one (Tonfa) exclusive to Monster Hunter Frontier G. Weapons are divided into two different classes: melee and ranged.</p>
                    </div>

                    <div class="articulo-boton justify-content-center">
                        <a class="btn btn-secondary" href="http://monsterhunter.wikia.com/wiki/Weapon_Types" target="blank" role="button">Leer más...</a>
                    </div>
                </div>

                <!--columna 3-->
                <div class="articulo col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 ">
                    <div class="articulo-imagen justify-content-center">
                        <img src="img/map.png">
                    </div>

                    <div class="articulo-texto justify-content-center" align="center">
                        <p><h3>Zonas</h3></p>
                        <p>Each different location features a different landscape, vegetation, and challenges to travel. Some areas have specific Hazards that make monster hunting more challenging and pose unique requirements. Different Large Monsters and Small Monsters...</p>
                    </div>

                    <div class="articulo-boton justify-content-center">
                        <a class="btn btn-secondary" href="http://monsterhunter.wikia.com/wiki/World_of_Monster_Hunter" target="blank" role="button">Leer más...</a>
                    </div>
                </div>
            </div>
        </div>