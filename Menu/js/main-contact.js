$(document).ready(function(){
    //Si hay más de 0 elementos textarea se iniciara.
    if($("textarea").length > 0){
        CKEDITOR.replaceAll(function(textarea,config) {
            if(textarea.className == "advanced"){
                config.removeButtons = "About";
                config.uploadUrl = "";
                return true;
            }/*else if(textarea.className == "basic"){

            }*/ //poner los else if necesarios para todos los tipos de CKEditor que hay.
        });
    }

    if($("select").length > 0){
        $(".weapon-select2").select2();
    }
});


 $(document).on("click", "#submit", function(event){

    /*Validador de input personalizado*/
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-záéíóúàèìòùäëïöüñ\s]+$/i.test(value);
    }, "Sólo se aceptan letras");


    $("#contact-form").validate({

        onfocusout: false,
        errorClass: "invalid",
        ignore: [], //Ignora que el elemento este en displayNone.

        rules:{

            name:{
                required: true,
                lettersonly: true,
                minlength: 2,
            },

            surname:{
                required: true,
                lettersonly: true,
                minlength: 2,
            },

            email:{
                required: true,
                email: true
            },

            mensajes:{
                //Asi se valida el CKEditor
                required: function(){
                    CKEDITOR.instances.mensajes.updateElement();
                },
                minlength: 1,
                maxlength: 420,
            }
        },

        messages:{
            
            name:{
                required: "El nombre es obligatorio",
                lettersonly: "Solamente se pueden introducir letras",
                minlength: "Introduzca como mínimo dos caracteres"
            },

            surname:{
                required: "Los apellidos son obligatorios",
                lettersonly: "Solamente se pueden introducir letras",
                minlength: "Introduzca como mínimo dos caracteres"
            },

            email:{
                required: "El email de contacto es obligatorio",
                email: "Introduzca una dirección de email valida"
            },

            mensajes:{
                maxlength: "El número máximo permitido de caracteres es de 420",
                minlength: "Como mínimo escriba 1 letra puto"
            }
        }
    });
    //Si el formulario es valido, es decir, esta validado, Se iniciara
    //el event.preventDefault(); y acto seguido el for, el cual
    //envia a la BD los datos del formulario.
    if($("#contact-form").valid()){
        //Poner el evento en el onclick. SIEMPRE(que sea necesario)
        event.preventDefault();

        var formData = new FormData($("#contact-form")[0]);
        //formData.append("form", "test-form"); Esto sirve para meter datos extra a la BD.

        for(var pair of formData.entries()){
            console.log(pair[0]+ ", "+pair[1]);
        };

      $.mockjax({
            url : "server.html",
            responseText : "BIEN ECHO",
        });

        $.ajax({
            type : "POST",
            url : "server.html",
            data : formData,
            processData: false,
            contentType: false,
            success : function(data){
                console.log("first Method Data Saved: " , data);
            },
            error : function(){
                console.log("Fisrt Method Data not Saved");
            }
        });
    }
 });