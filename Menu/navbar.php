<body>
    <!--Este contenedor tiene todos los elementos de la pagina dentro de el.-->
    <div class="container-fluid">
        <div class="jumbotron" id="cabecera">
            
            <!--Aqui esta el logo de la pagina y el nombre de la empresa que va debajo del logo.-->
            <div class="logo">
                <img src="img/mh.png" alt="Logo de la empresa">
            </div>

            <div class="title-page">
                <h1>Hunting Club</h1>
            </div>
            
            <!--Botones de menu de la pagina. Uno es un simple boton que redirige al suario a una pagina y el otro es un dropdown con un par de botones que no llevan a ningún sitio. -->
            <nav class="navbar navbar-expand-lg navbar-light" id="navbar">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="btn btn-secondary" href="http://monsterhunter.wikia.com/wiki/Monster_Hunter_Wiki" target="blank" role="button" id="secondary">Wiki oficial</a>

                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Menu
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="../Usuario/">Crear Usuario</a>
                            <a class="dropdown-item" href="../Blog/">Crear entrada</a>
                            <a class="dropdown-item" href="../Imagen/">Subir imagen</a>
                            <a class="dropdown-item" href="../Indice/">Indice</a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>