<?php

    require_once "UserController.php";
    require_once "Validation.php";

    $validation = new validation();
    $validation->isEmpty($_POST["name"], "name", "name");
    $validation->isEmpty($_POST["surname"], "surname", "surname");
    $validation->isEmpty($_POST["email"], "email", "email");
    $validation->minLength($_POST["name"], 2, "name", "name");
    $validation->maxLength($_POST["name"], 20, "name", "name");
    $validation->minLength($_POST["surname"], 2, "surname", "surname");
    $validation->maxLength($_POST["surname"], 21, "surname", "surname");
    $validation->minLength($_POST["email"], 5, "email", "email");
    $validation->maxLength($_POST["email"], 40, "email", "email");
    $validation->isEmail($_POST["email"], "email");

    if($validation->getValidation()){
        $user = new UserController($_POST);
        $alert['_validation'] = $validation->getValidation();
        $alert['message'] = $user->createUser($_POST);
        echo json_encode($alert);
    }else{
        echo json_encode($validation->getErrors());
    }
?>