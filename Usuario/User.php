<?php

    require_once("database/core/Database.php");

    class User extends Database {

        protected $_name;
        protected $_lastname;
        protected $_email;
        protected $_password;

        public function __construct(){
            parent::__construct();
        }

        public function getName(){
            return $this->_name;
        }

        public function setName($name){
            $this->_name = $name;
        }

        public function getLastname(){
            return $this->_lastname;
        }

        public function setLastname($lastname){
            $this->_lastname = $lastname;
        }

        public function getEmail(){
            return $this->_email;
        }

        public function setEmail($email){
            $this->_email = $email;
        }

        public function getPassword(){
            return $this->_password;
        }

        public function setPassword($password){
            $this->_password = $password;
        }

        public function indexUser(){
        
            $query =  "SELECT * 
            FROM `t_user`";
    
            $stmt = $this->_pdo->prepare($query);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
            return $result;
        }
    
        public function showUser($id){

            $query =  "SELECT * 
            FROM `t_user` 
            WHERE id = :id";
    
            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("id", $id);
            $stmt->execute();
            $result = $stmt->fetchAll();

            return $result;
        }

        public function createUser($user){

            $query = "insert into t_user (name, lastname, email, password) 
            values (:name, :surname, :email, :password)";
    
            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("name", $user['name']);
            $stmt->bindParam("surname", $user['surname']);
            $stmt->bindParam("email", $user['email']);
            $stmt->bindParam("password", $user['password']);
            $stmt->execute();

            $user_id = $this->_pdo->lastInsertId();
    
            return "Usuario añadido correctamente";
        }
    
        public function updateUser($user){

            $query = "update t_user set
            name = :name, 
            lastname = :lastname, 
            email = :email,  
            password = :password
            WHERE id = :id";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("name", $user['name']);
            $stmt->bindParam("lastname", $user['lastname']);
            $stmt->bindParam("email", $user['email']);
            $stmt->bindParam("password", $user['password']);
            $stmt->bindParam("id", $user['id']);
            $stmt->execute();

            return "Usuario actualizado correctamente";

        }
    
        public function deleteUser($id){

            $query =  "DELETE 
            FROM `t_user` 
            WHERE id = :id";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("id", $id);
            $stmt->execute();
            $result = $stmt->fetchAll();
            
            return $result;
            
        }
    }
?>