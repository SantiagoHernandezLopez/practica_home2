    <div class="container-fluid">
            <div class="jumbotron" id="cabecera">

                <div class="logo">
                    <img src="img/mh.png">
                </div>
                
                <!--Aqui esta el logo de la pagina y el nombre de la empresa que va debajo del logo.-->
                <div class="title-page">
                    <h1>Hunting Club</h1>
                </div>
                
                <!--Botones de menu de la pagina. Uno es un simple boton que redirige al suario a una pagina y el otro es un dropdown con un par de botones que no llevan a ningún sitio. -->
                <nav class="navbar navbar-expand-lg navbar-light" id="navbar">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <a class="btn btn-secondary" href="http://monsterhunter.wikia.com/wiki/Monster_Hunter_Wiki" target="blank" role="button" id="secondary">Wiki oficial</a>

                        <a class="btn btn-secondary" href="../Menu/" role="button" id="secondary">Menu</a>

                        <a class="btn btn-secondary" href="../Indice/" role="button" id="secondary">Indice</a>
                    </div>
                </nav>
            </div>
        </div>