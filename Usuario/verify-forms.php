<?php

    //Esto lo que hace es coger el archivo que crea el bojeto User y asegurarse que no falta nada.
    require_once "UserController.php";

    $alert["success"] = true;//Nuevo array llamado "message" que es true de base.

    if(empty($_POST["name"])){//Si "name" no tiene nada, "success" pasa a ser falso y sale el mensaje de error.
        
        array_push($alert, array(
            "message" => "El nombre es obligarotio"
        ));

        $alert["success"] = false;

    }else{

        if(strlen($_POST["name"]) < 2 || strlen($_POST["name"]) > 20){//Si la longitud de "name" es menos de 2 o mayor de 20, "success" pasa a ser falso y salta el mensaje de error.

            array_push($alert, array(
                "message" => "El nombre tiene que tener como minimo 2 caracteres y como maximo 20"
            ));

            $alert["success"] = false;

        }

    }
    
    if(empty($_POST["surname"])){

        array_push($alert, array(
            "message" => "El apellido es obligarotio"
        ));

        $alert["success"] = false;

    }else{

        if(strlen($_POST["surname"]) < 2 || strlen($_POST["surname"]) > 21){

            array_push($alert, array(
                "message" => "El apellido tiene que tener como minimo 2 caracteres y como maximo 21"
            ));

            $alert["success"] = false;

        }
    }
    
    if(empty($_POST["email"])){

        array_push($alert, array(
            "message" => "El email es obligarotio"
        ));

        $alert["success"] = false;

    }else{
        
        if(strlen($_POST["email"]) < 5 || strlen($_POST["email"]) > 40){

            array_push($alert, array(
                "message" => "El email tiene que tener como minimo 5 caracteres y como maximo 40"
            ));

            $alert["success"] = false;

        }

        if(strpos($_POST["email"], "@") === false){

            array_push($alert, array(
                "message" => "Ingrese una direccion de correo valida"
            ));

            $alert["success"] = false;
        }

    }
    
    if(strlen($_POST["mensajes"]) < 20 || strlen($_POST["mensajes"]) > 420){

        array_push($alert, array(
            "message" => "Escriba un comentario de minimo 20 caracteres y maximo 420"
        ));

        $alert["success"] = false;

    }
    
    if($alert["success"]){

        $user = new UserController($_POST);
        $alert["message"] = "Puede continuar " . $user->getUserName();

    }

    echo json_encode($alert);//echo devuelve un mensaje a ajax

?>
