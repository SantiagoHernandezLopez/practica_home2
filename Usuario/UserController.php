<?php

require_once("User.php");

//Creacion de un objeto User
class UserController {

    protected $_user;

    public function __construct(){
        $this->_user = new User();
    }

    public function indexUser(){

        return $this->_user->indexUser();
        
    }

    public function showUser($id){

        return $this->_user->showUser($id);
    }

    public function createUser($user){

        return $this->_user->createUser($user);
    }

    public function updateUser($user){

        return $this->_user->updateUser($user);
    }

    public function deleteUser($id){

        return $this->_user->deleteUser($id);
    }

}

?>