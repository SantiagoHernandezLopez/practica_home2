$(document).ready(function(){
    
    if($("select").length > 0){
        $(".weapon-select2").select2();
    }
});


 $(document).on("click", "#submit", function(event){

    event.preventDefault();

    var formData = new FormData($("#contact-form")[0]);

    for(var pair of formData.entries()){
        console.log(pair[0]+ ", "+pair[1]);
    };

    $.ajax({
        type : "POST",
        url : "UserRequest.php",
        dataType : "json",
        data : formData,
        cache : false,
        processData: false,
        contentType: false,

        success : function(alert){

            $(".error-alert").html('<ul class="errors"></ul>');
            if(alert._validation){

                $(".success").text("Bienvenido " + alert.message);
                $("#contact-form").fadeOut();
                $(".success-alert").removeClass("hidden").fadeOut().fadeIn();

            }else{

                $(".error-alert").removeClass("hidden");

                $.each(alert._errors, function(i, item){
                    if(item.message){
                        $(".errors").append("<li>" + item.message + "</li>");
                    }
                });

            }
        },

        error : function(alert){
            console.log("Fisrt Method Data not Saved");
        }
    });

    return false;
 });



// /*Validador de input personalizado*/
// jQuery.validator.addMethod("lettersonly", function(value, element) {
//     return this.optional(element) || /^[a-záéíóúàèìòùäëïöüñ\s]+$/i.test(value);
// }, "Sólo se aceptan letras");

//  $("#contact-form").validate({

//     onfocusout: false,
//     errorClass: "invalid",
//     ignore: [], //Ignora que el elemento este en displayNone.

//     rules:{

//         name:{
//             required: true,
//             lettersonly: true,
//             minlength: 2,
//             maxlength: 21
//         },

//         surname:{
//             required: true,
//             lettersonly: true,
//             minlength: 2,
//         },

//         email:{
//             required: true,
//             email: true,
//             minlength: 5,
//             maxlength: 40
//         },

//         mensajes:{
//             required: true,
//             minlength: 1,
//             maxlength: 420,
//         }
//     },

//     messages:{
        
//         name:{
//             required: "El nombre es obligatorio",
//             lettersonly: "Solamente se pueden introducir letras",
//             minlength: "Introduzca como mínimo dos caracteres",
//             maxlength: "Como máximo 20 caracteres"
//         },

//         surname:{
//             required: "Los apellidos son obligatorios",
//             lettersonly: "Solamente se pueden introducir letras",
//             minlength: "Introduzca como mínimo dos caracteres"
//         },

//         email:{
//             required: "El email de contacto es obligatorio",
//             email: "Introduzca una dirección de email valida",
//             minlength: "Como máximo 5 caracteres",
//             maxlength: "Como máximo 40 caracteres"
//         },

//         mensajes:{
//             maxlength: "El número máximo permitido de caracteres es de 420",
//             minlength: "Como mínimo escriba 1 letra puto"
//         }
//     }
// });