<?php

    require_once "ImageController.php";
    require_once "Validation.php";

    $validation = new validation();
    $validation->isEmpty($_POST["title"], "title", "title");
    $validation->isEmpty($_POST["url"], "url", "url");
    $validation->isEmpty($_POST["description"], "description", "description");
    $validation->isEmpty($_FILES["image"]["name"], "image", "image");
    $validation->minLength($_POST["title"], 2, "title", "title");
    $validation->maxLength($_POST["title"], 20, "title", "title");
    $validation->minLength($_POST["description"], 2, "description", "description");
    $validation->maxLength($_POST["description"], 20, "description", "description");
    $validation->Url($_POST["url"], "url");

    if($validation->getValidation()){
        $image = new ImageController($_POST, $_FILES);
        $alert['_validation'] = $validation->getValidation();
        $alert['message'] = $image->createImage($_POST, $_FILES);
        echo json_encode($alert);
    }else{
        echo json_encode($validation->getErrors());
    }
?>