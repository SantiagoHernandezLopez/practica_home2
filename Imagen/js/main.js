$(document).ready(function(){
    //Si hay más de 0 elementos textarea se iniciara.
    if($("textarea").length > 0){
        CKEDITOR.replaceAll(function(textarea,config) {
            if(textarea.className == "advanced"){
                config.removeButtons = "About";
                config.uploadUrl = "";
                return true;
            }/*else if(textarea.className == "basic"){

            }*/ //poner los else if necesarios para todos los tipos de CKEditor que hay.
        });
    }
});

$(document).on("click", "#submit", function(event){

    CKEDITOR.instances.description.updateElement();

    event.preventDefault();

    var formData = new FormData($("#contact-form")[0]);

    for(var pair of formData.entries()){
        console.log(pair[0]+ ", "+pair[1]);
    };

    $.ajax({
        type : "POST",
        url : "ImageRequest.php",
        dataType : "json",
        data : formData,
        cache : false,
        processData: false,
        contentType: false,

        success : function(alert){
            
            $(".error-alert").html('<ul class="errors"></ul>');
            if(alert._validation){

                $(".success").text("Imagen enviada");
                $("#contact-form").fadeOut();
                $(".success-alert").removeClass("hidden").fadeOut().fadeIn();

            }else{

                $(".error-alert").removeClass("hidden");

                $.each(alert._errors, function(i, item){
                    if(item.message){
                        $(".errors").append("<li>" + item.message + "</li>");
                    }
                });

            }
        },

        error : function(alert){
            console.log(alert);
        }
    });

    return false;
 });