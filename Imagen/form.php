    <div class="error-alert hidden">
        <ul class="errors"></ul>
    </div>

    <div class="success-alert hidden">
        <h1 class="success"></h1>
    </div>

    <form class="form col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="contact-form" method="get" action="">

        <div class="form-group">
            <label for="title">Titulo:</label>
            <input type="text" class="form-control" id="title" name="title"  placeholder="Titulo..." required>
        </div>

        <div class="form-group">
            <label for="image">Imagen:</label>
            <input type="file" class="form-control" id="image" name="image"  placeholder="Seleccione una imagen para subir..." accept="image/*">
        </div>

        <div class="form-group">
            <label for="description">Descripcion:</label>
            <textarea class="advanced" id="description" name="description"></textarea>
        </div>

        <div class="form-group">
            <label for="url">URL destino:</label>
            <input type="url" class="form-control" id="url" name="url" placeholder="URL de destino..." >
        </div>

        <button type="submit" class="btn btn-primary" id="submit">Submit</button>
    </form>