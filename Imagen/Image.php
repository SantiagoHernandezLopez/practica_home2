<?php

    require_once("database/core/Database.php");

    class Image extends Database {

        protected $_title;
        protected $_url;
        protected $_description;
        protected $_image;

        public function __construct(){
            parent::__construct();
        }

        public function getTitle(){
            return $this->_title;
        }

        public function setTitle($title){
            $this->_title = $title;
        }

        public function getUrl(){
            return $this->_content;
        }

        public function setUrl($Url){
            $this->_Url = $Url;
        }

        public function getDescription(){
            return $this->_description;
        }

        public function setDescription($description){
            $this->_description = $description;
        }

        public function getImage(){
            return $this->_image;
        }

        public function setImage($image){
            $this->_image = $image;
        }

        public function showArticle($id){

            $query =  "SELECT * 
            FROM `t_slider` 
            WHERE id = :id";
    
            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("id", $id);
    
            return $result;
        }

        public function uploadImage(){

            if(!move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $_FILES['image']['name'])){
                die('Error subiendo el archivo');
            }
            
            $img_name = basename($_FILES['image']['name']);
            return $img_name;
        }

        public function createImage($image){

            $image_url = $this->uploadImage();

            $query = "insert into t_slider (title, url, description, image_url) 
            values (:title, :url, :description, :image)";
    
            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("title", $image['title']);
            $stmt->bindParam("url", $image['url']);
            $stmt->bindParam("description", $image['description']);
            $stmt->bindParam("image", $image_url);
            $stmt->execute();
    
            return "Se ha añadido la imagen";
        }
    
        public function updateImage($image){
            
        }
    
        public function deleteImage($id){
            
        }
    }
?>