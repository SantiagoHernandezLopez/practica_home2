<?php

require_once("Image.php");

class ImageController {

    protected $_image;

    public function __construct(){
        $this->_image = new Image();
    }

    public function showImage($id){

        return $this->_image->showImage($id);
    }

    public function createImage($image){

        return $this->_image->createImage($image);
    }

    public function updateImage($image){

        return $this->_image->updateImage($image);
    }

    public function deleteImage($id){

        return $this->_image->deleteImage($id);
    }

}

?>