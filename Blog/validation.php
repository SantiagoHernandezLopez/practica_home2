<?php

class Validation {

    protected $_validation;
    protected $_errors;

    public function __construct(){
        $this->_validation = true;
        $this->_errors = array();
    }

    public function getValidation(){
        return $this->_validation;
    }

    public function getErrors(){
        return get_object_vars($this);
    }

    public function isEmpty($value, $input_id, $input_label){

        if(empty($value)){
        
            array_push($this->_errors, array(
                "message" => "Complete el campo " . $input_label,
                "id" => $input_id
            ));
    
            $this->_validation = false;
        }
    }

    public function minLength($value, $min_length, $input_id, $input_label){

        if(strlen($value) < $min_length){
        
            array_push($this->_errors, array(
                "message" => "El campo " . $input_label . " debe tener más de " . $min_length . " carácteres",
                "id" => $input_id
            ));
    
            $this->_validation = false;
        }
    }

    public function maxLength($value, $max_length, $input_id, $input_label){

        if(strlen($value) > $max_length){
        
            array_push($this->_errors, array(
                "message" => "El campo " . $input_label . " debe tener menos de " . $max_length . " carácteres",
                "id" => $input_id
            ));
    
            $this->_validation = false;
        }
    }
}

?>