<?php

    require_once("database/core/Database.php");

    class Article extends Database {

        protected $_title;
        protected $_content;
        protected $_category;

        public function __construct(){
            parent::__construct();
        }

        public function getTitle(){
            return $this->_title;
        }

        public function setTitle($title){
            $this->_title = $title;
        }

        public function getContent(){
            return $this->_content;
        }

        public function setContent($content){
            $this->_content = $content;
        }

        public function getCategory(){
            return $this->_category;
        }

        public function setCategory($category){
            $this->_category = $category;
        }

        public function showArticle($id){

            $query =  "SELECT * 
            FROM `t_article` 
            WHERE id = :id";
    
            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("id", $id);
    
            return $result;
        }

        public function createArticle($article){

            $query = "insert into t_article (title, content, categories) 
            values (:title, :coments, :categories)";
    
            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("title", $article['title']);
            $stmt->bindParam("coments", $article['coments']);
            $stmt->bindParam("categories", $article['categories']);
            $stmt->execute();
    
            return "Se ha añadido el articulo";
        }
    
        public function updateArticle($article){
            
        }
    
        public function deleteArticle($id){
            
        }
    }
?>