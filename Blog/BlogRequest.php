<?php

    require_once "BlogController.php";
    require_once "Validation.php";

    $validation = new validation();
    $validation->isEmpty($_POST["title"], "title", "title");
    $validation->isEmpty($_POST["coments"], "coments", "coments");
    $validation->minLength($_POST["title"], 2, "title", "title");
    $validation->maxLength($_POST["title"], 20, "title", "title");
    $validation->minLength($_POST["coments"], 5, "coments", "coments");
    $validation->maxLength($_POST["coments"], 40, "coments", "coments");

    if($validation->getValidation()){
        $article = new BlogController($_POST);
        $alert["_validation"] = $validation->getValidation();
        $alert["message"] = $article->createArticle($_POST);
        echo json_encode($alert);
    }else{
        echo json_encode($validation->getErrors());
    }
?>