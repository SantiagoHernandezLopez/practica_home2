    <body>   
        <div class="error-alert hidden">
            <ul class="errors"></ul>
        </div>

        <div class="success-alert hidden">
            <h1 class="success"></h1>
        </div>

        <form class="form col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="coments-form" method="get" action="">

            <div class="form-group">
                <label for="title">Titulo:</label>
                <input type="text" class="form-control" id="title" name="title"  placeholder="Titulo..." required>
            </div>

            <div class="form-group">
                <label for="categories">Categorias:</label>
                <select class="categories" name="categories" id="categories" multiple="multiple" required>
                    <optgroup label="Armas a Mele">
                        <option value="LS">Espada Larga</option>
                        <option value="HS">Espada Pesada</option>
                        <option value="SS">Espada y Escudo</option>
                        <option value="DB">Espadas Dobles</option>
                        <option value="H">Martillo</option>
                        <option value="HH">Cuerno de Caza</option>
                        <option value="L">Lanza</option>
                    </optgroup>

                    <optgroup label="Armas a distancia">
                        <option value="LB">Ballesta ligera</option>
                        <option value="MB">Ballesta Mediana</option>
                        <option value="HB">Ballesta Pesada</option>
                        <option value="B">Arco</option>
                    </optgroup>

                    <optgroup label="Armas Especiales">
                        <option value="GL">Lanza Pistola</option>
                        <option value="SA">Hacha Espada</option>
                        <option value="CB">Espada de Carga</option>
                        <option value="IG">Insect Glaive</option>
                        <option value="T">Tonfa</option>
                    </optgroup>
                </select>
            </div>

            <div class="form-group">
                <label for="coments">Comentarios: </label>
                <textarea class="advanced" id="coments" name="coments" placeholder="Escriba aqui su comentario sobre la categoria seleccionada..."></textarea>
            </div>

            <button type="submit" class="btn btn-primary" id="submit">Submit</button>
        </form>