$(document).ready(function(){
    if($("textarea").length > 0){
        CKEDITOR.replaceAll(function(textarea,config) {
            if(textarea.className == "advanced"){
                config.removeButtons = "About";
                config.uploadUrl = "";
                return true;
            }
        });
    }

    if($("select").length > 0){
        $(".categories").select2();
    }
});


 $(document).on("click", "#submit", function(event){

    CKEDITOR.instances.coments.updateElement();

    event.preventDefault();

    var formData = new FormData($("#coments-form")[0]);

    for(var pair of formData.entries()){
        console.log(pair[0]+ ", "+pair[1]);
    };

    $.ajax({
        type : "POST",
        url : "BlogRequest.php",
        dataType : "json",
        data : formData,
        cache : false,
        processData: false,
        contentType: false,

        success : function(alert){

            if(alert._validation){

                $(".error-alert").html('<ul class="errors"></ul>');
                console.log(alert);
                $(".success").text("Se ha añadido su comentario " + alert.message);
                $("#blog-form").fadeOut();
                $(".success-alert").removeClass("hidden").fadeOut().fadeIn();

            }else{

                $(".error-alert").removeClass("hidden");

                $.each(alert._errors, function(i, item){
                    if(item.message){
                        $(".errors").append("<li>" + item.message + "</li>");
                    }
                });

            }
        },

        error : function(alert){
            console.log(alert);
        }
    });

    return false;
 });