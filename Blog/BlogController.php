<?php

require_once("Blog.php");

class BlogController {

    protected $_article;

    public function __construct(){
        $this->_article = new Article();
    }

    public function showArticle($id){

        return $this->_article->showArticle($id);
    }

    public function createArticle($article){

        return $this->_article->createArticle($article);
    }

    public function updateArticle($article){

        return $this->_article->updateArticle($article);
    }

    public function deleteArticle($id){

        return $this->_article->deleteArticle($id);
    }
}

?>